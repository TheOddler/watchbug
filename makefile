
watch:
	@docker-compose -f "docker-compose.debug.yml" up

start:
	@docker-compose up

build:
	@docker-compose build

shell:
	docker run --rm -it -v $$(pwd):$$(pwd) -w $$(pwd)/app mcr.microsoft.com/dotnet/sdk:5.0 bash -c "dotnet tool restore; bash"
