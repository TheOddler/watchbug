using app.Models;
using Microsoft.EntityFrameworkCore;

namespace app
{
    public class AppDbContext : DbContext
    {
#pragma warning disable CS8618
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        public AppDbContext(DbContextOptions options) : base(options) { }
#pragma warning restore CS8618

        // protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {


        //     base.OnModelCreating(modelBuilder);
        // }
    }
}
