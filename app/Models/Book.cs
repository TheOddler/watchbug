namespace app.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        /// <summary>
        /// The author of this book as printed on the book itself.
        /// </summary>
        public Author Author { get; set; }
        public int AuthorId { get; set; }

#pragma warning disable CS8618
        private Book() { } // Just for EF
#pragma warning restore CS8618

        public Book(string title, Author author)
        {
            Title = title;
            Author = author;
            AuthorId = author.Id;
        }
    }
}