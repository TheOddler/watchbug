using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace app.Models
{
    public class Author
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        public ICollection<Book> Books { get; set; } = new List<Book>();

#pragma warning disable CS8618
        private Author() { } // Just for EF
#pragma warning restore CS8618

        public Author(string name)
        {
            Name = name;
        }
    }
}
