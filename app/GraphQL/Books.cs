using System.Linq;
using System.Threading.Tasks;
using app.Models;
using HotChocolate;
using HotChocolate.Data;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace app.GraphQL
{
    public partial class Query
    {
        [UseDbContext(typeof(AppDbContext))]
        [UseProjection] // If you use more than middleware, keep in mind that ORDER MATTERS. The correct order is UsePaging > UseProjection > UseFiltering > UseSorting
        public IQueryable<Book> GetBooks([ScopedService] AppDbContext db)
        {
            return db.Books;
        }
    }

    public partial class Mutation
    {
        [UseDbContext(typeof(AppDbContext))]
        public async Task<Book> CreateBookAsync(Book book, [ScopedService] AppDbContext db)
        {
            var newBook = (await db.AddAsync(book)).Entity;
            await db.SaveChangesAsync();
            return newBook;
        }
    }
}