# Now project without local dotnet clise:

* docker run --rm -it -v $(pwd):$(pwd) -w $(pwd) mcr.microsoft.com/dotnet/sdk:5.0 bash
* (optional) mkdir ./app && cd ./app
* dotnet new web
    * for more templates see: https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new
* add docker files, easiest through vscode docker plugin, run "Docker: Add Docker files to worksapce..."

# Entity Framework

* add migration: dotnet ef migrations add AddAuthorsAndBooks
* migrate: dotnet ef database update
